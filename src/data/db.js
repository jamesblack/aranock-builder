module.exports = {
  races: [
    {
      name: "Seyan'Du",
      skills: [
        {
          name: 'Braveness',
          base: 10,
          max: 90,
          difficulty: 6,
          divider: 20,
          type: 'STAT'
        },
        {
          name: 'Willpower',
          base: 10,
          max: 90,
          difficulty: 6,
          divider: 20,
          type: 'STAT'
        },
        {
          name: 'Intution',
          base: 10,
          max: 90,
          difficulty: 6,
          divider: 20,
          type: 'STAT'
        }
      ]
    }
  ]
}
